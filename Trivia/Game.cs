using System;
using System.Collections.Generic;
using System.Linq;

namespace Trivia
{
    public class Game
    {
        const int NB_CELLS = 12;
        private readonly List<Player> _players = new List<Player>();

        private readonly int[] _places = new int[6];
        private readonly int[] _purses = new int[6];

        private readonly bool[] _inPenaltyBox = new bool[6];

        private readonly LinkedList<string> _popQuestions = new LinkedList<string>();
        private readonly LinkedList<string> _scienceQuestions = new LinkedList<string>();
        private readonly LinkedList<string> _sportsQuestions = new LinkedList<string>();
        private readonly LinkedList<string> _rockQuestions = new LinkedList<string>();

        private int _currentPlayerPosition;
        private bool _isGettingOutOfPenaltyBox;

        public Game()
        {
            for (var i = 0; i < 50; i++)
            {
                _popQuestions.AddLast(CreateQuestion(Category.Pop, i));
                _scienceQuestions.AddLast(CreateQuestion(Category.Science, i));
                _sportsQuestions.AddLast(CreateQuestion(Category.Sports, i));
                _rockQuestions.AddLast(CreateQuestion(Category.Rock, i));
            }
        }

        private string CreateQuestion(Category category, int index)
        {
            return category + " Question " + index;
        }

        public bool Add(Player player)
        {
            _players.Add(player);
            _places[HowManyPlayers()] = 0;
            _purses[HowManyPlayers()] = 0;
            _inPenaltyBox[HowManyPlayers()] = false;

            Console.WriteLine(player.name + " was added");
            Console.WriteLine("They are player number " + _players.Count);
            player.position = _players.Count() - 1;
            return true;
        }

        private int HowManyPlayers()
        {
            return _players.Count;
        }

        public void Roll(int roll)
        {
            Console.WriteLine(_players[_currentPlayerPosition].name + " is the current player");
            Console.WriteLine("They have rolled a " + roll);

            if (_inPenaltyBox[_currentPlayerPosition])
            {
                if (roll % 2 != 0)
                {
                    _isGettingOutOfPenaltyBox = true;

                    Console.WriteLine(_players[_currentPlayerPosition].name + " is getting out of the penalty box");
                    NewPlayerLocation(roll);
                }
                else
                {
                    Console.WriteLine(_players[_currentPlayerPosition].name + " is not getting out of the penalty box");
                    _isGettingOutOfPenaltyBox = false;
                }
            }
            else
            {
                NewPlayerLocation(roll);
            }
        }

        private void NewPlayerLocation(int roll)
        {
            _places[_currentPlayerPosition] = CurrentPlayerLocation() + roll;
            if (CurrentPlayerLocation() >= NB_CELLS)
            {
                _places[_currentPlayerPosition] = CurrentPlayerLocation() - NB_CELLS;
            }

            print();
            AskQuestion();
        }

        private void print()
        {
            Console.WriteLine(_players[_currentPlayerPosition].name
                                + "'s new location is "
                                + CurrentPlayerLocation());
            Console.WriteLine("The category is " + CurrentCategory());
        }

        private void AskQuestion()
        {
            switch (CurrentCategory())
            {

                case Category.Pop:
                    Console.WriteLine(_popQuestions.First());
                    _popQuestions.RemoveFirst();
                    return;
                case Category.Science:
                    Console.WriteLine(_scienceQuestions.First());
                    _scienceQuestions.RemoveFirst();
                    return;
                case Category.Sports:
                    Console.WriteLine(_sportsQuestions.First());
                    _sportsQuestions.RemoveFirst();
                    return;
                case Category.Rock:
                    Console.WriteLine(_rockQuestions.First());
                    _rockQuestions.RemoveFirst();
                    return;
                default:
                    break;
            }
        }

        private Category CurrentCategory()
        {
            if (CurrentPlayerLocation() == 0) return Category.Pop;
            if (CurrentPlayerLocation() == 4) return Category.Pop;
            if (CurrentPlayerLocation() == 8) return Category.Pop;
            if (CurrentPlayerLocation() == 1) return Category.Science;
            if (CurrentPlayerLocation() == 5) return Category.Science;
            if (CurrentPlayerLocation() == 9) return Category.Science;
            if (CurrentPlayerLocation() == 2) return Category.Sports;
            if (CurrentPlayerLocation() == 6) return Category.Sports;
            if (CurrentPlayerLocation() == 10) return Category.Sports;
            return Category.Rock;
        }

        private int CurrentPlayerLocation()
        {
            return _places[_currentPlayerPosition];
        }

        public bool WasCorrectlyAnswered()
        {
            if (_inPenaltyBox[_currentPlayerPosition])
            {
                if (_isGettingOutOfPenaltyBox)
                {
                    Console.WriteLine("Answer was correct!!!!");
                    _purses[_currentPlayerPosition]++;
                    Console.WriteLine(_players[_currentPlayerPosition].name
                            + " now has "
                            + _purses[_currentPlayerPosition]
                            + " Gold Coins.");

                    var winner = DidPlayerWin();
                    _currentPlayerPosition++;
                    if (_currentPlayerPosition == _players.Count) _currentPlayerPosition = 0;

                    return winner;
                }
                else
                {
                    _currentPlayerPosition++;
                    if (_currentPlayerPosition == _players.Count) _currentPlayerPosition = 0;
                    return true;
                }
            }
            else
            {
                Console.WriteLine("Answer was corrent!!!!");
                _purses[_currentPlayerPosition]++;
                Console.WriteLine(_players[_currentPlayerPosition].name
                        + " now has "
                        + _purses[_currentPlayerPosition]
                        + " Gold Coins.");

                var winner = DidPlayerWin();
                _currentPlayerPosition++;
                if (_currentPlayerPosition == _players.Count) _currentPlayerPosition = 0;

                return winner;
            }
        }

        public bool WrongAnswer()
        {
            Console.WriteLine("Question was incorrectly answered");
            Console.WriteLine(_players[_currentPlayerPosition].name + " was sent to the penalty box");
            _inPenaltyBox[_currentPlayerPosition] = true;

            _currentPlayerPosition++;
            if (_currentPlayerPosition == _players.Count) _currentPlayerPosition = 0;
            return true;
        }


        private bool DidPlayerWin()
        {
            return !(_purses[_currentPlayerPosition] == 6);
        }
    }

}
