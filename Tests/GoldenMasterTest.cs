using ApprovalTests;
using ApprovalTests.Reporters;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Trivia
{
    public class GoldenMasterTest
    {
        private static bool _notAWinner;


        [Fact]
        [UseReporter(typeof(DiffReporter))]
        public void GoldenMaster()
        {
            var fakeconsole = new StringWriter();
            Console.SetOut(fakeconsole);

            var game = new Game();

			var rollsSequence = new Queue<int>(new List<int>()
	{
		6,9,2,4,7,0,5,1,0,2,8,4,2,3,8,1,2,5,1,7,8,4,5,7,8,1,3,1,9,2,9,6,2,2,1,0,2,6,1,2,8,9,7,1,3,5,2,5,1,3,
	});

			game.Add(new Player("Chet"));
			game.Add(new Player("Pat"));
			game.Add(new Player("Sue"));

			do
			{
				game.Roll(rollsSequence.Dequeue());

				if (rollsSequence.Dequeue() == 7)
				{
					_notAWinner = game.WrongAnswer();
				}
				else
				{
					_notAWinner = game.WasCorrectlyAnswered();
				}

			} while (_notAWinner);

            Approvals.Verify(fakeconsole.ToString());
        }
    }
}