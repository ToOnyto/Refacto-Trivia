# Liste des étapes pour le refacto dans l'ordre de difficulté


- Implémenter un approval test pour être sur de ne rien casser dans le code déjà existant ✅
- Remplacer les valeurs en brut par des constantes ✅
- Supprimer la méthode IsPlayable(), car elle n'est jamais utilisée ✅
- Changer visiblité de la méthode HowManyPlayers ✅
- Implementer des méthodes similaire a CreateRockQuestion() pour les autres types de questions ✅
- Implémenter une classe Category avec une Enum pour les types de questions ✅
- Modifier la méthode CurrentCategory() en implémentant un switch pour la visibilité/performance
- Modifier la méthode CurrentCategory() et AskQuestion() pour qu'elles puissent fonctionner avec la classe Category ✅
- Implémenter une méthode print() pour le result du move et retirer la duplication du code ✅
- Implémenter un switch dans la méthode AskQuestion() pour gagner en performance/visibilité ✅
- Supprimer la duplication de code dans la méthode Roll() en implémentant une nouvelle méthode NewPlayerLocation(roll) ✅
- Supprimer la duplication de _places[_currentPlayer] en implémentant une nouvelle méthode CurrentPlayerLocation() ✅
- Implémenter la classe Player et ses méthodes
- Implémenter la classe Questions et ses méthodes
- Trouver le bug et le résoudre
---
## Problèmes rencontrés lors du refacto du projet

- Problèmes d'implémentation de la position du joueur, qui entrait en conflit avec le position du joueur sur le plateau de jeu.
